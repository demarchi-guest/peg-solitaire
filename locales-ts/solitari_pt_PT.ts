<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_PT">
<context>
    <name>Traduccio</name>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="817"/>
        <source>English</source>
        <translatorcomment>O nome do seu idioma</translatorcomment>
        <translation>Portuguese</translation>
    </message>
</context>
<context>
    <name>frmPrincipal</name>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="79"/>
        <location filename="../scr/frmprincipal.cpp" line="214"/>
        <location filename="../scr/frmprincipal.cpp" line="486"/>
        <location filename="../scr/frmprincipal.cpp" line="875"/>
        <location filename="../scr/frmprincipal.cpp" line="3971"/>
        <source>Solitari</source>
        <translation>Solitario</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="140"/>
        <location filename="../scr/frmprincipal.cpp" line="500"/>
        <location filename="../scr/frmprincipal.cpp" line="2752"/>
        <source>Solucions</source>
        <translation>Soluciones</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="215"/>
        <location filename="../scr/frmprincipal.cpp" line="3972"/>
        <source>No s&apos;ha trobat l&apos;arxiu %1</source>
        <translation>En el se ha encontrado él archivo %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="123"/>
        <location filename="../scr/frmprincipal.cpp" line="499"/>
        <location filename="../scr/frmprincipal.cpp" line="3794"/>
        <source>Modalitats del joc</source>
        <translatorcomment>Games</translatorcomment>
        <translation>Jogos</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3808"/>
        <source>Solitari 3x5</source>
        <translation>Solitário 3x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3809"/>
        <location filename="../scr/frmprincipal.cpp" line="3847"/>
        <source>Triangular 4x7</source>
        <translation>Triangular 4x7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3819"/>
        <source>Quadrat 5x5</source>
        <translation>Quadrado 5x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3825"/>
        <source>Wiegleb</source>
        <translation>Wiegleb</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3826"/>
        <location filename="../scr/frmprincipal.cpp" line="3891"/>
        <source>Diamant 9x9</source>
        <translatorcomment>Diamond 9x9</translatorcomment>
        <translation>Diamante 9x9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3831"/>
        <location filename="../scr/frmprincipal.cpp" line="3845"/>
        <source>Quadrat 6x6</source>
        <translation>Quadrado 6x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3832"/>
        <source>Diamant 5x5</source>
        <translation>Diamante 5x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3833"/>
        <source>Diamant 7x7</source>
        <translation>Diamante 7x7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3835"/>
        <source>Incomplet 6x6</source>
        <translation>Incompleto 6x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3836"/>
        <source>Incomplet 7x7</source>
        <translation>Incompleto 7x7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3837"/>
        <location filename="../scr/frmprincipal.cpp" line="3868"/>
        <source>Wiegleb reduit</source>
        <translation>Wiegleb reduzido</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3838"/>
        <source>Solitari 8x9</source>
        <translation>Solitário 8x9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3839"/>
        <location filename="../scr/frmprincipal.cpp" line="3844"/>
        <source>Solitari 5x6</source>
        <translation>Solitário 5x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3851"/>
        <location filename="../scr/frmprincipal.cpp" line="3880"/>
        <source>Solitari 7x5</source>
        <translation>Solitário 7x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3853"/>
        <source>Quadrat 9x9</source>
        <translation>Quadrado 9x9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3855"/>
        <source>Triangular 5</source>
        <translation>Triangular 5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3856"/>
        <source>Triangular 4</source>
        <translation>Triangular 4</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3857"/>
        <source>Triangular 6</source>
        <translation>Triangular 6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3869"/>
        <source>Solitari 3x5 bis</source>
        <translation>Solitário 3x5 bis</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3870"/>
        <source>Solitari 4x4</source>
        <translation>Solitário 4x4</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3871"/>
        <source>Solitari 6x5</source>
        <translation>Solitário 6x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3872"/>
        <source>Solitari 4x5</source>
        <translation>Solitário 4x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3873"/>
        <source>Triangular 7</source>
        <translation>Trianguilar 7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3874"/>
        <source>Triangular 8</source>
        <translation>Trianguilar 8</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3875"/>
        <source>Triangular 9</source>
        <translation>Trianguilar 9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3877"/>
        <source>Clàssic - molinet</source>
        <translation>Clássico - molinillo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3878"/>
        <source>Triangular 10</source>
        <translation>Trianguilar 10</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3879"/>
        <source>Quadrat 8x8</source>
        <translation>Quadrado 8x8</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="154"/>
        <location filename="../scr/frmprincipal.cpp" line="501"/>
        <location filename="../scr/frmprincipal.cpp" line="3496"/>
        <location filename="../scr/frmprincipal.cpp" line="3648"/>
        <location filename="../scr/frmprincipal.cpp" line="3692"/>
        <source>Jocs personalitzats</source>
        <translatorcomment>Custom games</translatorcomment>
        <translation>Jogos personalizados</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3801"/>
        <source>Solitari estrella 7x7</source>
        <translatorcomment>Star solitaire 7x7</translatorcomment>
        <translation>Solitário estrela</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3813"/>
        <source>Solitari 6x7</source>
        <translation>Solitário 6x7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="464"/>
        <location filename="../scr/frmprincipal.cpp" line="516"/>
        <source>&amp;Surt</source>
        <translation>&amp;Fecha</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="465"/>
        <source>Veure records</source>
        <translation>Ver marcas pessoais</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="466"/>
        <location filename="../scr/frmprincipal.cpp" line="484"/>
        <location filename="../scr/frmprincipal.cpp" line="571"/>
        <location filename="../scr/frmprincipal.cpp" line="650"/>
        <source>Ajuda</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="467"/>
        <location filename="../scr/frmprincipal.cpp" line="568"/>
        <source>Credits</source>
        <translation>Créditos</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="468"/>
        <location filename="../scr/frmprincipal.cpp" line="592"/>
        <source>Agraïments</source>
        <translatorcomment>Gratefulnesses</translatorcomment>
        <translation>Agradecimentos</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="469"/>
        <location filename="../scr/frmprincipal.cpp" line="574"/>
        <source>Web del programa</source>
        <translation>Site do programa</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="470"/>
        <location filename="../scr/frmprincipal.cpp" line="577"/>
        <source>Web del tangram</source>
        <translation>Site do tangram</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="474"/>
        <source>Avança</source>
        <translatorcomment>Redo</translatorcomment>
        <translation>Avançar</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="491"/>
        <location filename="../scr/frmprincipal.cpp" line="562"/>
        <source>Inici solució</source>
        <translation>Início da solução</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="532"/>
        <location filename="../scr/frmprincipal.cpp" line="714"/>
        <source>Pausa</source>
        <translation>Pausa</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="533"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="714"/>
        <source>Continua</source>
        <translation>Contínua</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2972"/>
        <source>Joc resolt!</source>
        <translation>Xogo resolto!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2975"/>
        <source>Nova marca personal</source>
        <translation>Nova marca persoal</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2976"/>
        <source>Heu establert un nou record personal en aquesta modalitat de joc</source>
        <translation>Conseguiu unha nova marca persoal nesta modalidade do xogo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2986"/>
        <location filename="../scr/frmprincipal.cpp" line="2991"/>
        <source>Moviment %1 de %2 </source>
        <translation>Movemento %1 de %2</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="481"/>
        <location filename="../scr/frmprincipal.cpp" line="631"/>
        <source>Programa</source>
        <translation>Programa</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="482"/>
        <location filename="../scr/frmprincipal.cpp" line="643"/>
        <source>&amp;Moviments joc</source>
        <translation>&amp;Movimentos do jogo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="483"/>
        <location filename="../scr/frmprincipal.cpp" line="647"/>
        <source>Idioma</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="517"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="488"/>
        <location filename="../scr/frmprincipal.cpp" line="521"/>
        <source>Veure marques personals</source>
        <translation>Ver as marcas pessoais</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="424"/>
        <location filename="../scr/frmprincipal.cpp" line="3614"/>
        <source>Joc carregat. El vostre record actual és:  %1</source>
        <translation>Jogo carregado. Vossa marca pessoal actual é: %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="489"/>
        <location filename="../scr/frmprincipal.cpp" line="524"/>
        <source>Elimina marques personals</source>
        <translation>Elimina as marcas pessoais</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="490"/>
        <location filename="../scr/frmprincipal.cpp" line="527"/>
        <source>Reinicia el joc actual</source>
        <translation>Reinicia o jogo actual</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="528"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="554"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="559"/>
        <source>Shift+Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3810"/>
        <source>Europeu</source>
        <translatorcomment>European</translatorcomment>
        <translation>Europeu</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3823"/>
        <source>Quadrat 5x5 - central</source>
        <translatorcomment>Square 5x5 - central</translatorcomment>
        <translation>Quadrado 5x5 - central</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3824"/>
        <source>Quadrat 5x5 - H</source>
        <translatorcomment>Square 5x5 - H</translatorcomment>
        <translation>Quadrado 5x5 - H</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3827"/>
        <source>Europeu - creu</source>
        <translatorcomment>European - cross</translatorcomment>
        <translation>Europeu - cruz</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3846"/>
        <source>Quadrat 5x5 - quadrats</source>
        <translatorcomment>Square 5x5 - square</translatorcomment>
        <translation>Quadrado 5x5 - quadrados</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3848"/>
        <source>Triangular 4x7 - quadrat</source>
        <translatorcomment>Triangular 4x7 - square</translatorcomment>
        <translation>Triangular 4x7 - quadrado</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3858"/>
        <source>Wiegleb - creu petita</source>
        <translatorcomment>Wiegleb - small cross</translatorcomment>
        <translation>Wiegleb - cruz pequena</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3859"/>
        <source>Wiegleb - simetria</source>
        <translatorcomment>Wiegleb - symmetry</translatorcomment>
        <translation>Wiegleb - simetria</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3882"/>
        <source>Clàssic - O</source>
        <translation>Clásico - O</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3884"/>
        <source>Dos quadrats 10x10</source>
        <translatorcomment>Two square 10x10</translatorcomment>
        <translation>Dois quadrados 10x10</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3885"/>
        <source>Dos quadrats 11x11</source>
        <translatorcomment>Two square  11x11</translatorcomment>
        <translation>Dois quadrados  11x11</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3886"/>
        <source>Tres quadrats 16x16</source>
        <translatorcomment>Three square 16x16</translatorcomment>
        <translation>Três quadrados 16x16</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3887"/>
        <source>Dos quadrats 9x9</source>
        <translatorcomment>Two square  9x9</translatorcomment>
        <translation>Dois quadrados  9x9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3888"/>
        <source>Tres quadrats 13x13</source>
        <translatorcomment>Three square  13x13</translatorcomment>
        <translation>Três quadrados 13x13</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3889"/>
        <source>Quatre quadrats 13x13</source>
        <translatorcomment>Four square 13x13</translatorcomment>
        <translation>Quatro quadrados 13x13</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3890"/>
        <source>Clàssic ampliat</source>
        <translatorcomment>Expanded Classic</translatorcomment>
        <translation>Clássico ampliado</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3892"/>
        <source>Rombe 36</source>
        <translatorcomment>Rhombus 36</translatorcomment>
        <translation>Losango 36</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3897"/>
        <source>Hexagonal 7x11</source>
        <translatorcomment>Hexagonal 7x11</translatorcomment>
        <translation>Hexagonal 7x11</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3905"/>
        <source>Solitari a l&apos;atzar</source>
        <translatorcomment>Random solitaire</translatorcomment>
        <translation>Solitário a esmo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3818"/>
        <source>Solitari OK</source>
        <translation>Solitário OK</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="883"/>
        <source>Sota llicència GPL 2.0 o posterior</source>
        <translation>Baixo licença GPL 3.0 ou posterior</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1008"/>
        <source>De debó voleu eliminar les vostres marques?</source>
        <translation>Seguro que quer eliminar suas marcas pessoais?</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1164"/>
        <location filename="../scr/frmprincipal.cpp" line="1170"/>
        <location filename="../scr/frmprincipal.cpp" line="1530"/>
        <location filename="../scr/frmprincipal.cpp" line="2129"/>
        <source>Cercant solució</source>
        <translation>Procurando solução</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1368"/>
        <location filename="../scr/frmprincipal.cpp" line="1650"/>
        <location filename="../scr/frmprincipal.cpp" line="1656"/>
        <source>No ha estat possible trobar una solució!</source>
        <translation>Não se encontrou uma solução!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2620"/>
        <location filename="../scr/frmprincipal.cpp" line="2670"/>
        <source>S&apos;ha trobat una nova solució!</source>
        <translation>Encontrou-se uma nova solução!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2721"/>
        <source>Solució %1</source>
        <translation>Solução %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2762"/>
        <source>De debó voleu carregar la solució? Perdreu els moviments que heu fet!</source>
        <translation>Seguro que quer carregar a solução? Vai perder os movimentos realizados!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2769"/>
        <source>. Feu servir els botons Avança i Retrocedeix per veure la solució. </source>
        <translation>. Utilize os botões Avança e Retrocede para ver a solução.</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2897"/>
        <source> de %1</source>
        <translation> de %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2969"/>
        <source>No hi ha més moviments: el joc ha finalitzat!</source>
        <translation>Non hai máis movementos: finalizou o xogo!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2997"/>
        <source>No hi ha moviments!</source>
        <translation>Não há movimentos!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3795"/>
        <source>Clàssic</source>
        <translation>Clásico</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3796"/>
        <source>Clàssic - simetria</source>
        <translatorcomment>Classic - symmetry</translatorcomment>
        <translation>Clássico - simetria</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3797"/>
        <source>Clàssic - pentàgon</source>
        <translatorcomment>Classic - pentagon</translatorcomment>
        <translation>Clássico - pentágono </translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3798"/>
        <source>Clàssic - creu petita</source>
        <translatorcomment>Classic - small cross</translatorcomment>
        <translation>Clássico - cruz pequena</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3799"/>
        <source>Clàssic - creu gran</source>
        <translatorcomment>Classic - big cross</translatorcomment>
        <translation>Clássico - cruz grande</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3802"/>
        <source>Clàssic - superior</source>
        <translatorcomment>Classic - top</translatorcomment>
        <translation>Clássico - superior</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3803"/>
        <source>Clàssic - inferior</source>
        <translatorcomment>Classic - lower</translatorcomment>
        <translation>Clássico - inferior</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3804"/>
        <source>Clàssic - fletxa</source>
        <translatorcomment>Classic - arrow</translatorcomment>
        <translation>Clássico - seta</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3805"/>
        <source>Clàssic - piràmide</source>
        <translatorcomment>Classic - pyramid</translatorcomment>
        <translation>Clássico - pirâmide</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3806"/>
        <source>Clàssic - diamant</source>
        <translatorcomment>Classic - diamond</translatorcomment>
        <translation>Clássico - diamante</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3807"/>
        <source>Clàssic - rombe</source>
        <translation>Clássico - rombo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3811"/>
        <location filename="../scr/frmprincipal.cpp" line="3815"/>
        <location filename="../scr/frmprincipal.cpp" line="3863"/>
        <source>Asimètric 8x8</source>
        <translation>Asimétrico 8x8</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3814"/>
        <source>Asimètric - superior</source>
        <translatorcomment>Asymmetric - top</translatorcomment>
        <translation>Asimetrico - superior</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3816"/>
        <source>Clàssic - central</source>
        <translatorcomment>Classic - central</translatorcomment>
        <translation>Clássico - central</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3820"/>
        <location filename="../scr/frmprincipal.cpp" line="3881"/>
        <source>Clàssic - quadrat central</source>
        <translatorcomment>Classic - central square</translatorcomment>
        <translation>Clássico - quadrado central</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3821"/>
        <source>Clàssic - rectangle central</source>
        <translatorcomment>Classic - central rectangle</translatorcomment>
        <translation>Clássico - rectangulo central</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3822"/>
        <source>Clàssic - arbre</source>
        <translatorcomment>Classic - tree</translatorcomment>
        <translation>Clássico - árvore</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3828"/>
        <source>Wiegleb - clàssic</source>
        <translatorcomment>Wiegleb - classic</translatorcomment>
        <translation>Wiegleb - clássico </translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3830"/>
        <source>Solitari 6x6</source>
        <translation>Solitário 6x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3834"/>
        <source>Anglès antic</source>
        <translation>Inglês antigo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3840"/>
        <source>Wiegleb - fletxa</source>
        <translation>Wiegleb - seta</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3841"/>
        <source>Clàssic - E</source>
        <translatorcomment>Classic - E</translatorcomment>
        <translation>Clássico - E</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3842"/>
        <source>Clàssic - R</source>
        <translatorcomment>Classic - R</translatorcomment>
        <translation>Clássico - R</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3843"/>
        <source>Clàssic - T</source>
        <translatorcomment>Classic - T</translatorcomment>
        <translation>Clássico - T</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3849"/>
        <source>Triangular 4x7 - piràmide</source>
        <translatorcomment>Triangular 4x7 - pyramid</translatorcomment>
        <translation>Triangular 4x7 - pirâmide</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3850"/>
        <source>Quadrat 5x5 - piràmide</source>
        <translatorcomment>Square 5x5 - pyramid</translatorcomment>
        <translation>Quadrado 5x5 - pirâmide</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3852"/>
        <location filename="../scr/frmprincipal.cpp" line="3862"/>
        <source>Asimètric 6x6</source>
        <translation>Asimétrico 6x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3854"/>
        <source>Anglès antic - diamant</source>
        <translatorcomment>Old English -  diamond</translatorcomment>
        <translation>Inglês antigo - diamante</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3861"/>
        <source>Clàssic - quadrat</source>
        <translatorcomment>Classic - square</translatorcomment>
        <translation>Clássico - quadrado</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3865"/>
        <source>Clàssic - cúpula</source>
        <translatorcomment>Classic - dome</translatorcomment>
        <translation>Clássico - cúpula</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3867"/>
        <source>Clàssic - Cabana</source>
        <translatorcomment>Classic - cabin</translatorcomment>
        <translation>Clássico - cabaña</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3876"/>
        <source>Europeu - quadrat</source>
        <translation>Europeu - quadrado</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3894"/>
        <source>Hexagonal inclinat</source>
        <translation>Hexagonal inclinado</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3895"/>
        <source>Clàssic - 4 forquilles</source>
        <translatorcomment>Classic  - four forks</translatorcomment>
        <translation>Clássico - 4 forquillas</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3896"/>
        <source>Pentagonal</source>
        <translation>Pentagonal</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3898"/>
        <source>Clàssic - Dos quadrats</source>
        <translatorcomment>Classic - two square</translatorcomment>
        <translation>Clássico - dois quadrados</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3899"/>
        <source>Clàssic - Banyes</source>
        <translatorcomment>Classic - horns</translatorcomment>
        <translation>Clássico - cornos</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3900"/>
        <source>Clàssic - X</source>
        <translatorcomment>Classic - X</translatorcomment>
        <translation>Clássico - X </translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3901"/>
        <source>Clàssic - Torxa</source>
        <translatorcomment>Classic - torch</translatorcomment>
        <translation>Clássico - tocha</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3902"/>
        <source>Clàssic - Palau</source>
        <translatorcomment>Classic - palace</translatorcomment>
        <translation>Clássico - palácio</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3904"/>
        <source>Personalitzat</source>
        <translatorcomment>Custom game</translatorcomment>
        <translation>Personalizado</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3908"/>
        <source>Solo</source>
        <translation>Solo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3909"/>
        <source>Solitari 8x3</source>
        <translation>Solitário 8x3</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3910"/>
        <source>Solitari 8x6</source>
        <translation>Solitário 8x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="473"/>
        <source>Retrocedeix</source>
        <translatorcomment>Undo</translatorcomment>
        <translation>Retroceder</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="476"/>
        <location filename="../scr/frmprincipal.cpp" line="492"/>
        <location filename="../scr/frmprincipal.cpp" line="580"/>
        <source>Resol</source>
        <translation>Resolve</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="819"/>
        <source>&amp;%1 %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="890"/>
        <source>Credits del %1</source>
        <translation>Créditos do %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="945"/>
        <location filename="../scr/frmprincipal.cpp" line="951"/>
        <source>Marques personals</source>
        <translation>Marcas pessoais</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1007"/>
        <source>Elimina les marques personals</source>
        <translation>Elimina as marcas pessoais</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1164"/>
        <location filename="../scr/frmprincipal.cpp" line="1170"/>
        <location filename="../scr/frmprincipal.cpp" line="1531"/>
        <location filename="../scr/frmprincipal.cpp" line="2130"/>
        <location filename="../scr/frmprincipal.cpp" line="2385"/>
        <location filename="../scr/frmprincipal.cpp" line="2435"/>
        <location filename="../scr/frmprincipal.cpp" line="2527"/>
        <location filename="../scr/frmprincipal.cpp" line="2700"/>
        <source>Atura</source>
        <translation>Para</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2385"/>
        <source>Desant dades</source>
        <translation>Guardando os dados</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2407"/>
        <source>Desant dades: %1 de %2</source>
        <translation>Guardando os dados: %1 de %2</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2435"/>
        <location filename="../scr/frmprincipal.cpp" line="2527"/>
        <location filename="../scr/frmprincipal.cpp" line="2700"/>
        <source>Carregant dades</source>
        <translation>Carregando os dados</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2736"/>
        <location filename="../scr/frmprincipal.cpp" line="2961"/>
        <source>No hi ha solucions</source>
        <translation>Não há soluções</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2761"/>
        <source>Carregar </source>
        <translation>Carregar </translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2768"/>
        <source>S&apos;ha carregat la </source>
        <translation>Carregou-se a </translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2897"/>
        <source>Moviment </source>
        <translation>Movimento </translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3633"/>
        <source>Cap joc personalitzat</source>
        <translation>Sem jogos personalizados</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3771"/>
        <source> - invers</source>
        <translation> - inverso</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3774"/>
        <source> - diagonal</source>
        <translation> - diagonal</translation>
    </message>
</context>
</TS>
