<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_GB">
<context>
    <name>Traduccio</name>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="817"/>
        <source>English</source>
        <translation>English</translation>
    </message>
</context>
<context>
    <name>frmPrincipal</name>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="79"/>
        <location filename="../scr/frmprincipal.cpp" line="214"/>
        <location filename="../scr/frmprincipal.cpp" line="486"/>
        <location filename="../scr/frmprincipal.cpp" line="875"/>
        <location filename="../scr/frmprincipal.cpp" line="3971"/>
        <source>Solitari</source>
        <translation>Peg-solitaire</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="140"/>
        <location filename="../scr/frmprincipal.cpp" line="500"/>
        <location filename="../scr/frmprincipal.cpp" line="2752"/>
        <source>Solucions</source>
        <translation>Solutions</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="215"/>
        <location filename="../scr/frmprincipal.cpp" line="3972"/>
        <source>No s&apos;ha trobat l&apos;arxiu %1</source>
        <translation>The file %1 does not exist</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="123"/>
        <location filename="../scr/frmprincipal.cpp" line="499"/>
        <location filename="../scr/frmprincipal.cpp" line="3794"/>
        <source>Modalitats del joc</source>
        <translation>Games</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3808"/>
        <source>Solitari 3x5</source>
        <translation>Peg-solitaire 3x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3809"/>
        <location filename="../scr/frmprincipal.cpp" line="3847"/>
        <source>Triangular 4x7</source>
        <translation>Triangular 4x7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3819"/>
        <source>Quadrat 5x5</source>
        <translation>Square 5x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3825"/>
        <source>Wiegleb</source>
        <translation>Wiegleb</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3826"/>
        <location filename="../scr/frmprincipal.cpp" line="3891"/>
        <source>Diamant 9x9</source>
        <translation>Diamond 9x9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3831"/>
        <location filename="../scr/frmprincipal.cpp" line="3845"/>
        <source>Quadrat 6x6</source>
        <translation>Square 6x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3832"/>
        <source>Diamant 5x5</source>
        <translation>Diamond 5x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3833"/>
        <source>Diamant 7x7</source>
        <translation>Diamond 7x7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3835"/>
        <source>Incomplet 6x6</source>
        <translation>Incomplete 6x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3836"/>
        <source>Incomplet 7x7</source>
        <translation>Incomplete 7x7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3837"/>
        <location filename="../scr/frmprincipal.cpp" line="3868"/>
        <source>Wiegleb reduit</source>
        <translation>Wiegleb reduced</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3838"/>
        <source>Solitari 8x9</source>
        <translation>Peg-Solitaire 8x9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3839"/>
        <location filename="../scr/frmprincipal.cpp" line="3844"/>
        <source>Solitari 5x6</source>
        <translation>Peg-Solitaire 5x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3851"/>
        <location filename="../scr/frmprincipal.cpp" line="3880"/>
        <source>Solitari 7x5</source>
        <translation>Peg-solitarie 7x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3853"/>
        <source>Quadrat 9x9</source>
        <translation>Square 9x9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3855"/>
        <source>Triangular 5</source>
        <translation>Triangular 5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3856"/>
        <source>Triangular 4</source>
        <translation>Triangular 4</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3857"/>
        <source>Triangular 6</source>
        <translation>Triangular 6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3869"/>
        <source>Solitari 3x5 bis</source>
        <translation>Peg-solitaire 3x5 bis</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3870"/>
        <source>Solitari 4x4</source>
        <translation>Peg-solitaire 4x4</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3871"/>
        <source>Solitari 6x5</source>
        <translation>Peg-solitaire 6x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3872"/>
        <source>Solitari 4x5</source>
        <translation>Peg-solitaire 4x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3873"/>
        <source>Triangular 7</source>
        <translation>Triangular 7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3874"/>
        <source>Triangular 8</source>
        <translation>Triangular 8</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3875"/>
        <source>Triangular 9</source>
        <translation>Triangular 9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3877"/>
        <source>Clàssic - molinet</source>
        <translation>Classic - small mill</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3878"/>
        <source>Triangular 10</source>
        <translation>Triangular 10</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3879"/>
        <source>Quadrat 8x8</source>
        <translation>Square 8x8</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="154"/>
        <location filename="../scr/frmprincipal.cpp" line="501"/>
        <location filename="../scr/frmprincipal.cpp" line="3496"/>
        <location filename="../scr/frmprincipal.cpp" line="3648"/>
        <location filename="../scr/frmprincipal.cpp" line="3692"/>
        <source>Jocs personalitzats</source>
        <translation>Custom games</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3801"/>
        <source>Solitari estrella 7x7</source>
        <translation>Peg-solitari star 7x7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3813"/>
        <source>Solitari 6x7</source>
        <translation>Peg-solitaire 6x7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="464"/>
        <location filename="../scr/frmprincipal.cpp" line="516"/>
        <source>&amp;Surt</source>
        <translation>&amp;Quit</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="465"/>
        <source>Veure records</source>
        <translation>View personal records</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="466"/>
        <location filename="../scr/frmprincipal.cpp" line="484"/>
        <location filename="../scr/frmprincipal.cpp" line="571"/>
        <location filename="../scr/frmprincipal.cpp" line="650"/>
        <source>Ajuda</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="467"/>
        <location filename="../scr/frmprincipal.cpp" line="568"/>
        <source>Credits</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="468"/>
        <location filename="../scr/frmprincipal.cpp" line="592"/>
        <source>Agraïments</source>
        <translation>Acknowledgements</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="469"/>
        <location filename="../scr/frmprincipal.cpp" line="574"/>
        <source>Web del programa</source>
        <translation>Program Web</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="470"/>
        <location filename="../scr/frmprincipal.cpp" line="577"/>
        <source>Web del tangram</source>
        <translation>Tangram web</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="474"/>
        <source>Avança</source>
        <translation>Redo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="491"/>
        <location filename="../scr/frmprincipal.cpp" line="562"/>
        <source>Inici solució</source>
        <translation>Home Solution</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="532"/>
        <location filename="../scr/frmprincipal.cpp" line="714"/>
        <source>Pausa</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="533"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="714"/>
        <source>Continua</source>
        <translation>Continues</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2972"/>
        <source>Joc resolt!</source>
        <translation>Solved game!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2975"/>
        <source>Nova marca personal</source>
        <translation>New personal record</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2976"/>
        <source>Heu establert un nou record personal en aquesta modalitat de joc</source>
        <translation>You have a new personal record in this modality of the game</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2986"/>
        <location filename="../scr/frmprincipal.cpp" line="2991"/>
        <source>Moviment %1 de %2 </source>
        <translation>Jump %1 of %2</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="481"/>
        <location filename="../scr/frmprincipal.cpp" line="631"/>
        <source>Programa</source>
        <translation>Program</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="482"/>
        <location filename="../scr/frmprincipal.cpp" line="643"/>
        <source>&amp;Moviments joc</source>
        <translation>Game &amp;moves</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="483"/>
        <location filename="../scr/frmprincipal.cpp" line="647"/>
        <source>Idioma</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="517"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="488"/>
        <location filename="../scr/frmprincipal.cpp" line="521"/>
        <source>Veure marques personals</source>
        <translation>View personal records</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="424"/>
        <location filename="../scr/frmprincipal.cpp" line="3614"/>
        <source>Joc carregat. El vostre record actual és:  %1</source>
        <translation>Game loaded. Your current personal record: %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="489"/>
        <location filename="../scr/frmprincipal.cpp" line="524"/>
        <source>Elimina marques personals</source>
        <translation>Delete personal records</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="490"/>
        <location filename="../scr/frmprincipal.cpp" line="527"/>
        <source>Reinicia el joc actual</source>
        <translation>Restarts game</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="528"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="554"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="559"/>
        <source>Shift+Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3810"/>
        <source>Europeu</source>
        <translation>European</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3823"/>
        <source>Quadrat 5x5 - central</source>
        <translation>Square 5x5 - central</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3824"/>
        <source>Quadrat 5x5 - H</source>
        <translation>Square 5x5 - H</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3827"/>
        <source>Europeu - creu</source>
        <translation>European - cross</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3846"/>
        <source>Quadrat 5x5 - quadrats</source>
        <translation>Square 5x5 - square</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3848"/>
        <source>Triangular 4x7 - quadrat</source>
        <translation>Triangular 4x7 - square</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3858"/>
        <source>Wiegleb - creu petita</source>
        <translation>Wiegleb - small cross</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3859"/>
        <source>Wiegleb - simetria</source>
        <translation>Wiegleb - symmetry</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3882"/>
        <source>Clàssic - O</source>
        <translation>Classic - O</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3884"/>
        <source>Dos quadrats 10x10</source>
        <translation>Two square 10x10</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3885"/>
        <source>Dos quadrats 11x11</source>
        <translation>Two square  11x11</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3886"/>
        <source>Tres quadrats 16x16</source>
        <translation>Three square 16x16</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3887"/>
        <source>Dos quadrats 9x9</source>
        <translation>Two square  9x9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3888"/>
        <source>Tres quadrats 13x13</source>
        <translation>Three square  13x13</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3889"/>
        <source>Quatre quadrats 13x13</source>
        <translation>Four square 13x13</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3890"/>
        <source>Clàssic ampliat</source>
        <translation>Expanded Classic</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3892"/>
        <source>Rombe 36</source>
        <translation>Rhombus 36</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3897"/>
        <source>Hexagonal 7x11</source>
        <translation>Hexagonal 7x11</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3905"/>
        <source>Solitari a l&apos;atzar</source>
        <translation>Random Solitaire</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3818"/>
        <source>Solitari OK</source>
        <translation>Ok Peg-solitaire</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="883"/>
        <source>Sota llicència GPL 2.0 o posterior</source>
        <translation>Under GPL 3.0 or later</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1008"/>
        <source>De debó voleu eliminar les vostres marques?</source>
        <translation>Sure you want to delete their personal records?</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1164"/>
        <location filename="../scr/frmprincipal.cpp" line="1170"/>
        <location filename="../scr/frmprincipal.cpp" line="1530"/>
        <location filename="../scr/frmprincipal.cpp" line="2129"/>
        <source>Cercant solució</source>
        <translation>Looking for solution</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1368"/>
        <location filename="../scr/frmprincipal.cpp" line="1650"/>
        <location filename="../scr/frmprincipal.cpp" line="1656"/>
        <source>No ha estat possible trobar una solució!</source>
        <translation>Not found a solution!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2620"/>
        <location filename="../scr/frmprincipal.cpp" line="2670"/>
        <source>S&apos;ha trobat una nova solució!</source>
        <translation>We found a new solution!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2721"/>
        <source>Solució %1</source>
        <translation>Solution %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2762"/>
        <source>De debó voleu carregar la solució? Perdreu els moviments que heu fet!</source>
        <translation>Sure you want to load the solution? Will lose the movements!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2769"/>
        <source>. Feu servir els botons Avança i Retrocedeix per veure la solució. </source>
        <translation>. Use the Redo and Undo buttons to see the solution.</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2897"/>
        <source> de %1</source>
        <translation> of %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2969"/>
        <source>No hi ha més moviments: el joc ha finalitzat!</source>
        <translation>There are no more movements: the game has finalized!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2997"/>
        <source>No hi ha moviments!</source>
        <translatorcomment>There are no more movements!</translatorcomment>
        <translation>There are no more movements!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3795"/>
        <source>Clàssic</source>
        <translation>Classic</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3796"/>
        <source>Clàssic - simetria</source>
        <translation>Classic - symmetry</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3797"/>
        <source>Clàssic - pentàgon</source>
        <translation>Classic - pentagon</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3798"/>
        <source>Clàssic - creu petita</source>
        <translation>Classic - small cross</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3799"/>
        <source>Clàssic - creu gran</source>
        <translation>Classic - big cross</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3802"/>
        <source>Clàssic - superior</source>
        <translation>Classic - top</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3803"/>
        <source>Clàssic - inferior</source>
        <translation>Classic - lower</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3804"/>
        <source>Clàssic - fletxa</source>
        <translation>Classic - arrow</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3805"/>
        <source>Clàssic - piràmide</source>
        <translation>Classic - pyramid</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3806"/>
        <source>Clàssic - diamant</source>
        <translation>Classic - diamond</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3807"/>
        <source>Clàssic - rombe</source>
        <translation>Classic - rhomb</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3811"/>
        <location filename="../scr/frmprincipal.cpp" line="3815"/>
        <location filename="../scr/frmprincipal.cpp" line="3863"/>
        <source>Asimètric 8x8</source>
        <translation>Asymmetric  8x8</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3814"/>
        <source>Asimètric - superior</source>
        <translation>Asymmetric - top</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3816"/>
        <source>Clàssic - central</source>
        <translation>Classic - central</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3820"/>
        <location filename="../scr/frmprincipal.cpp" line="3881"/>
        <source>Clàssic - quadrat central</source>
        <translation>Classic - central square</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3821"/>
        <source>Clàssic - rectangle central</source>
        <translation>Classic - central rectangle</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3822"/>
        <source>Clàssic - arbre</source>
        <translation>Classic - tree</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3828"/>
        <source>Wiegleb - clàssic</source>
        <translation>Wiegleb - classic</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3830"/>
        <source>Solitari 6x6</source>
        <translation>Peg-solitaire 6x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3834"/>
        <source>Anglès antic</source>
        <translation>Old English</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3840"/>
        <source>Wiegleb - fletxa</source>
        <translation>Wiegleb - arrow</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3841"/>
        <source>Clàssic - E</source>
        <translation>Classic - E</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3842"/>
        <source>Clàssic - R</source>
        <translation>Classic - R</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3843"/>
        <source>Clàssic - T</source>
        <translation>Classic - T</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3849"/>
        <source>Triangular 4x7 - piràmide</source>
        <translation>Triangular 4x7 - pyramid</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3850"/>
        <source>Quadrat 5x5 - piràmide</source>
        <translation>Square 5x5 - pyramid</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3852"/>
        <location filename="../scr/frmprincipal.cpp" line="3862"/>
        <source>Asimètric 6x6</source>
        <translation>Asymmetric 6x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3854"/>
        <source>Anglès antic - diamant</source>
        <translation>Old English -  diamond</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3861"/>
        <source>Clàssic - quadrat</source>
        <translation>Classic - square</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3865"/>
        <source>Clàssic - cúpula</source>
        <translation>Classic - dome</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3867"/>
        <source>Clàssic - Cabana</source>
        <translation>Classic - cabin</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3876"/>
        <source>Europeu - quadrat</source>
        <translation>European - square</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3894"/>
        <source>Hexagonal inclinat</source>
        <translation>Hex inclined</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3895"/>
        <source>Clàssic - 4 forquilles</source>
        <translation>Classic  - four forks</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3896"/>
        <source>Pentagonal</source>
        <translation>Pentagonal</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3898"/>
        <source>Clàssic - Dos quadrats</source>
        <translation>Classic - two square</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3899"/>
        <source>Clàssic - Banyes</source>
        <translation>Classic - horns</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3900"/>
        <source>Clàssic - X</source>
        <translation>Classic  - X</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3901"/>
        <source>Clàssic - Torxa</source>
        <translation>Classic - torch</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3902"/>
        <source>Clàssic - Palau</source>
        <translation>Classic - palace</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3904"/>
        <source>Personalitzat</source>
        <translation>Custom</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3908"/>
        <source>Solo</source>
        <translation>Solo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3909"/>
        <source>Solitari 8x3</source>
        <translatorcomment>Peg-solitaire 8x3</translatorcomment>
        <translation>Peg-solitaire 8x3</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3910"/>
        <source>Solitari 8x6</source>
        <translatorcomment>Peg-solitaire 8x6</translatorcomment>
        <translation>Peg-solitaire 8x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="473"/>
        <source>Retrocedeix</source>
        <translation>Undo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="476"/>
        <location filename="../scr/frmprincipal.cpp" line="492"/>
        <location filename="../scr/frmprincipal.cpp" line="580"/>
        <source>Resol</source>
        <translation>Solve</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="819"/>
        <source>&amp;%1 %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="890"/>
        <source>Credits del %1</source>
        <translation>About %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="945"/>
        <location filename="../scr/frmprincipal.cpp" line="951"/>
        <source>Marques personals</source>
        <translation>Personal records</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1007"/>
        <source>Elimina les marques personals</source>
        <translation>Delete personal records</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1164"/>
        <location filename="../scr/frmprincipal.cpp" line="1170"/>
        <location filename="../scr/frmprincipal.cpp" line="1531"/>
        <location filename="../scr/frmprincipal.cpp" line="2130"/>
        <location filename="../scr/frmprincipal.cpp" line="2385"/>
        <location filename="../scr/frmprincipal.cpp" line="2435"/>
        <location filename="../scr/frmprincipal.cpp" line="2527"/>
        <location filename="../scr/frmprincipal.cpp" line="2700"/>
        <source>Atura</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2385"/>
        <source>Desant dades</source>
        <translation>Saving data</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2407"/>
        <source>Desant dades: %1 de %2</source>
        <translation>Saving data: %1 of %2</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2435"/>
        <location filename="../scr/frmprincipal.cpp" line="2527"/>
        <location filename="../scr/frmprincipal.cpp" line="2700"/>
        <source>Carregant dades</source>
        <translation>Loading data</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2736"/>
        <location filename="../scr/frmprincipal.cpp" line="2961"/>
        <source>No hi ha solucions</source>
        <translation>No solutions found</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2761"/>
        <source>Carregar </source>
        <translation>Upload </translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2768"/>
        <source>S&apos;ha carregat la </source>
        <translation>Is loaded </translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2897"/>
        <source>Moviment </source>
        <translation>Movement </translation>
    </message>
    <message>
        <source> de </source>
        <translation type="vanished"> of </translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3633"/>
        <source>Cap joc personalitzat</source>
        <translation>Without custom games</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3771"/>
        <source> - invers</source>
        <translation> - reverse</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3774"/>
        <source> - diagonal</source>
        <translation> - diagonal</translation>
    </message>
</context>
</TS>
