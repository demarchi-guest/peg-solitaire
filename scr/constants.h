/***********************************************************************
 *
 * Copyright (C) 2010-2017 Innocent De Marchi <tangram.peces@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/
#ifndef CONSTANTS_H
#define CONSTANTS_H

enum EstatsDelJoc {
        joc = 0x0,
        solucio = 0x1
     };

//struct variables_recerca_solucio {
//    int valorMinimMovimentsJoc;
//    int valorMinimIndexDeMovimentsPerMovimentsMassius;
//    int valorMaximIndexDeMovimentsPerMovimentsMassius;
//    int maximNumeroDeConjunts;
//    int maximNumeroDeFitxesAillades;
//    int minimIndexDeMoviments;
//    int minimNumeroDeConjuntsActual;
//    int minimNumeroDeFitxesAilladesActual;
//    int minimIndexMovimentsPerFiltrarMovimentsSeleccionats;
//} ;

/*
  Directoris
 */
const QString DIRECTORIS_LINUX="/usr/share/games/peg-solitaire";
const QString DIRECTORIS_LINUX_PROGRAMA="/games";
const QString DIRECTORIS_LINUX_AUXILIARS="/usr/share/games/peg-solitaire";
const QString DIRECTORI_ARXIU_GAMES_SOL="/games/data.sol";
const QString DIRECTORI_IMATGES="images";
const QString DIRECTORI_LOCALES="locales";
const QString DIRECTORI_DADES="games";
const QString DIR_HELP="help";

const QString ADRECA_WEB_REGISTRE="http://peg-solitaire.sourceforge.net/registre.html";
const QString ADRECA_WEB_PROGRAMA_1="http://peg-solitaire.sourceforge.net/";
const QString ADRECA_WEB_PROGRAMA_2="http://sourceforge.net/projects/peg-solitaire/";
const QString ADRECA_WEB_PROGRAMA_PECES="http://pecesjocdetangr.sourceforge.net/";
const QString ADRECA_CORREU="tangram.peces@gmail.com";


#endif // CONSTANTS_H
