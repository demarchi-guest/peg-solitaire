/***********************************************************************
 *
 * Copyright (C) 2010-2017 Innocent De Marchi <tangram.peces@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/
#ifndef MOVIMENT_H
#define MOVIMENT_H

#include <QUndoCommand>

class Tauler;
class Fitxa;

class Moviment : public QUndoCommand
{
    Q_GADGET
public:
    Moviment(int fitxaInicial, int fitxaBotada, int fitxaFinal, int direccioMoviment, Tauler *taulerJoc);


        virtual void  redo();// moviment Endevant;
        virtual void undo(); //moviment Enrrera;

 void fesMoviment(int primera, int segona, int tercera);

 void eliminaFitxesVermelles();

 /* Quan es fan moviments enrrera/envant
  * cal assegurar-se que no hi ha cap moviment
  * "marcat"
  */
 void eliminaMovimentMarcat();


 QString movimentACoordenades();
 /*És per poder seguir el joc amb una solució carregada
  * Es pot marcar les fitxes, etc...*/
 void marcaSituacioJoc();

private:
    int p_fitxaInicial;
    int p_fitxaBotada;
    int p_fitxaFinal;
    int p_direccioMoviment;
    Tauler *p_taulerJoc;
};

#endif // MOVIMENT_H
