/***********************************************************************
 *
 * Copyright (C) 2010-2017 Innocent De Marchi <tangram.peces@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/
#ifndef FITXA_H
#define FITXA_H

#include <QGraphicsEllipseItem>

class Tauler;

class Fitxa : public QGraphicsEllipseItem
{
    Q_GADGET
public:
    Fitxa( Tauler*  taulerJoc, QGraphicsItem* parent=0);

   int estat() const{
        return m_estat;
            }

   QPoint coordenades() const;

   int tipusMoviment() const{
        return p_tipusMoviment;}

   int preferenciaMoure() const{
       return p_preferenciaMoure; }

   int nivellAillament() const{
       return p_nivellAillament;}

   //Conserva l'index (del QHash del tauler) de les fitxes botada i final dels
   //possibles moviments d'aquesta fitxa
   QList <QPoint> movimentsPossibles;


   void calculaMovimentsPosiblesFitxa();

   //Conserva les coordenades de les fitxes destí de la fitxa actual
   QList<QPoint> movimentsFitxa;
   void calculaMovimentsFitxa();

   bool fitxaFinalJoc() const{
        return p_fitxaFinalJoc;}

   bool fitxaUsadaGeneracioSolitariAtzar() const{
       return p_fitxaUsadaGeneracioSolitariAtzar;}

   void setFitxaSolitariPersonalitzat(bool valor);
   bool fitxaSolitariPersonalitzat() const{
       return p_fitxaSolitariPersonalitzat;}

   void setConjuntAillament(int numConjunt);
   void setDistanciaAillament(int distancia);
   void setDistanciaAillamentConjunt(int distancia);

   int conjuntAillament()const{
       return p_numConjunt;}

   int distanciaAillament() const{
       return p_distanciaAillament;}

   int distanciaAillamentConjunt() const{
       return p_distanciaAillamentConjunt;}


    public slots:

    //Propietats

    /*
      Controla l'estat de la fitxa:
     -2: No dibuixa la fitxa 10/02/13 No es fa servir això
     -1: No hi ha fitxa
      0: Buit
      10: Buit final de joc
      1: Estat normal
      11: Estat normal final de joc
      2: Fitxa seleccionada
      3: Destí d'un moviment
      4: Joc resolt (verd)
      5: El joc s'ha acabat ja que no hi ha més moviments (vermell)
      6: Dibuixa un quadrat en la personalització/modificacíó
         Serà un joc sense fitxa (estat -1)
      */
    void setEstat (int nouValor, bool canviarFitxaFinalJoc=false);

    void setCoordenades (QPoint coordenades);
 /*
           Controla el tipus de moviment
           1: moviment directa (l'habitual del joc)
           2: moviment invers
           3: moviment en direcció perpendicular i obliqua
           4: proposta de problema 10/02/13 ¿? No es fa servir això
           */
    void setTipusMoviment (int nouTipus);

    void ferMoviment();

    bool esPotMoure();

    /* Les fitxes amb preferencia més petita
     * van primer per moure
     */
    void setPreferenciaMoure(int preferencia);

    void setEsMou(bool valor);

    void setFitxaAillada(bool valor);

    void setNivellAillament( int valor);

    void incrementaNivellAillament();

    void setFitxaFinalJoc(bool valor);
    //Marca les fitxes que s'ha fet servir en la
    //generació d'un solitari a l'atzar
    void setfitxaUsadaGeneracioSolitariAtzar(bool valor);


protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option, QWidget *widget);

    virtual void mousePressEvent(QGraphicsSceneMouseEvent* event);

private:
   //Estat de la fitxa per pintar-la
   int m_estat;

   //Coordenades de la fitxa en el joc
   QPoint m_coordenades;


   /*Tipus de moviment del joc
    * 1 clàssic
    * 2 invers
    * 3 diagonal
    */
   int p_tipusMoviment;

   //Indica la preferència de la fitxa
   int p_preferenciaMoure;

   //Indica el nivell d'aillament actual de la fitxa
   int p_nivellAillament;


   //Controla si la fitxa està aïllada
   bool p_aillada;

   //controla si la fitxa s'ha mogut en algun moment
   bool p_esMou;

   //Controla si la fitxa és una posició final
   //del joc
   bool p_fitxaFinalJoc;

   //Controla si és una fitxa que s'ha fet servir
   //en la generació d'un solitari a l'atzar
   bool p_fitxaUsadaGeneracioSolitariAtzar;

   //Controla si la fitxa està en la
   //generació de solitari personalitzat
   bool p_fitxaSolitariPersonalitzat;

   int p_numConjunt;

   int p_distanciaAillament;

   int p_distanciaAillamentConjunt;


   //Tauler de joc al qual pertany la fitxa
   Tauler* m_taulerJoc;

};

#endif // FITXA_H
