/***********************************************************************
 *
 * Copyright (C) 2010-2017 Innocent De Marchi <tangram.peces@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/
#ifndef RELLOTGE_H
#define RELLOTGE_H

#include <QtGui>
#include <QLabel>


class QTimer;

class Rellotge : public QLabel
{
    Q_OBJECT

public:
    Rellotge(QWidget * parent = 0, Qt::WindowFlags f = 0 );

    void iniciaRellotge();
    void reIniciaRellotge();
    void aturaRellotge();
    /*
      Si tipus=0 retorna el temps amb format
      tipus=1 retorna nomès el temps
    */
    QString retornaTemps(int tipus=0);
    void estableixTemps(QString temps);

    bool rellotgeEnMarxa();

    int get_minuts() const{
        return minuts;
            }
    int get_segons() const{
        return segons;
            }    
//    int get_milisegons10() const{
//        return milisegons10;
//            }

    int retornarSegonsTotals();

private slots:
    void actualitzaRellotge();

private:
    QTimer *rellotgeQTimer;
    int hores,minuts,segons,milisegons10;

    bool p_rellotgeEnMarxa;


};

#endif // RELLOTGE_H
