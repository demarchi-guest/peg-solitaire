/***********************************************************************
 *
 * Copyright (C) 2010-2017 Innocent De Marchi <tangram.peces@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/
#ifndef FUNCIONS_H
#define FUNCIONS_H

#include <QtGui>

/*
  Directoris linux
 */
QString directoriLinux();

/*
  Número de fitxes de la configuració passada per parámetre
  */
int numeroConfiguracio(QString config);

QString coordenadesAMoviment(QString coordenades);

/* fa una pausa en milisegons*/
//void pausa(int temps);


#endif // FUNCIONS_H
